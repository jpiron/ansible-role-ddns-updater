# Ansible role ddns-updater
This role aims to install and configure [ddns-updater](https://github.com/philippmeisberger/ddns-updater).

## Dependencies
None.

## Role variables
See defaults/main.yml for available configuration items and default values.
